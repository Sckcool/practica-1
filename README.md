# Practica 1
1a Diodo y 1b fuente rectificada

introduccion; vemos como se comporta la corriente alterna al ser puesta en diodos y despues como se rectifica la onda
en un puente de diodos, para conseguir que la onda de corriente alterna se estabilize en corriente directa.
Asi podimos ver como es el comportamiento del factor de rizo para la rectificación de la corriente AC.

Material:

protoboard, 
transformador,
caimanes,
multimetro,
laptop(para ver la señal de onda).

Practica 1a:

diodo in40001,
resistencia 1.5k ohms.

Practica 1b :

puente de diodos, 
regulador LM317,
2 capacitores de 10 microF,
potenciometro de 10kohms,
resistencia 220 ohms.

